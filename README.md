About
-----
Vostok is a small set of customizations for the Android Open Source Project.
Unlike many other projects, it's just a set of patches on top of AOSP. Only
a few [own repositories](https://gitlab.com/groups/vostok) have been added.

Supported Devices
-----------------
| Name                    | Codename  | Branch            | Build              |
| ----------------------- | --------- | ----------------- | ------------------ |

Setting Up Development System
-----------------------------
Install prerequisites:

    apt update
    apt install --yes openjdk-8-jdk-headless git-core gnupg flex bison bc gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev lib32z-dev libxml2-utils xsltproc unzip python3 libssl-dev kmod
    mkdir ~/bin
    export PATH=$PATH:$HOME/bin
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo

Configure Git:

    git config --global user.name "Your Name"
    git config --global user.email "you@example.com"

Enable [ccache](https://ccache.samba.org) to speed up subsequent builds:

    export CCACHE_EXEC=/usr/bin/ccache
    export USE_CCACHE=1

Building
--------
Select the branch and codename of your device from the table above.

Download the source code:

    mkdir android
    cd android
    repo init -u https://android.googlesource.com/platform/manifest -b {BRANCH}
    mkdir -p .repo/local_manifests
    curl https://vostok.gitlab.io/vostok-10/vostok.xml > .repo/local_manifests/vostok.xml
    repo sync -c

You'll need a lot of disk space and some patience.

Change current directory to the Android source tree root and build:

    source build/envsetup.sh    # add all needed commands into PATH
    lunch aosp_{CODENAME}-user  # select the target device
    ./vostok-10/apply           # apply customizations
    make dist -j `nproc`        # build OS (target files and fastboot image)

It will take some time to compile all the bits of the operating system. Output
images will be signed with test keys. Never flash those images! This is a huge
security risk because those keys are well-known.

You must generate your own set of release keys and keep them private (do this
only once):

    ./vostok-10/genkeys ../release-keys

Re-sign the images with your release keys:

    ./vostok-10/sign ../release-keys

You'll find the output in `out/dist/{BUILD}-{MMDD}.fastboot.zip`. Time to run
it!

Running
-------
Unlock the bootloader of your device, boot it into fastboot mode and flash the
images:

    fastboot update {BUILD}-{MMDD}.fastboot.zip

The device will reboot automatically. Welcome to your new system!
